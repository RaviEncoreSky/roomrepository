package com.example.roomdatabaseexample;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.example.roomdatabaseexample.baseActivity.BaseAppActivity;
import com.example.roomdatabaseexample.databinding.ActivityAddTaskBinding;
import com.example.roomdatabaseexample.roomDatabaseFiles.DatabaseClient;
import com.example.roomdatabaseexample.roomDatabaseFiles.Task;

public class AddTaskActivity extends BaseAppActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ActivityAddTaskBinding addTaskBinding = ActivityAddTaskBinding.inflate(getLayoutInflater());
        setContentView(addTaskBinding.getRoot());

        addTaskBinding.buttonSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String sTask = addTaskBinding.editTextTask.getText().toString().trim();
                final String sDesc = addTaskBinding.editTextDesc.getText().toString().trim();
                final String sFinishBy = addTaskBinding.editTextFinishBy.getText().toString().trim();

                if (sTask.isEmpty()) {
                    addTaskBinding.editTextTask.setError("Task required");
                    addTaskBinding.editTextTask.requestFocus();
                    return;
                }

                if (sDesc.isEmpty()) {
                    addTaskBinding.editTextDesc.setError("Desc required");
                    addTaskBinding.editTextDesc.requestFocus();
                    return;
                }

                if (sFinishBy.isEmpty()) {
                    addTaskBinding.editTextFinishBy.setError("Finish by required");
                    addTaskBinding.editTextFinishBy.requestFocus();
                    return;
                }
                saveTask(sTask,sDesc,sFinishBy);
            }
        });
    }

    private void saveTask(String sTask,String sDesc, String sFinishBy) {

        class SaveTask extends AsyncTask<Void, Void, Void> {

            @Override
            protected Void doInBackground(Void... voids) {

                //creating a task
                Task task = new Task();
                task.setTask(sTask);
                task.setDesc(sDesc);
                task.setFinishBy(sFinishBy);
                task.setFinished(false);

                //adding to database
                DatabaseClient.getInstance(getApplicationContext()).getAppDatabase()
                        .taskDao()
                        .insert(task);
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                finish();
                startActivity(new Intent(getApplicationContext(), RoomDashboard.class));
                Toast.makeText(getApplicationContext(), "Data Saved Successfully", Toast.LENGTH_LONG).show();
            }
        }

        SaveTask st = new SaveTask();
        st.execute();
    }
}