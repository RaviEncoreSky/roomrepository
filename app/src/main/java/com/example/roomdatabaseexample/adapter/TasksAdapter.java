package com.example.roomdatabaseexample.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.roomdatabaseexample.UpdateTaskActivity;
import com.example.roomdatabaseexample.databinding.ItemTaskListBinding;
import com.example.roomdatabaseexample.roomDatabaseFiles.Task;

import java.util.List;

public class TasksAdapter extends RecyclerView.Adapter<TasksAdapter.TasksViewHolder> {

    private Context mCtx;
    private List<Task> taskList;

    public TasksAdapter(Context mCtx, List<Task> taskList) {
        this.mCtx = mCtx;
        this.taskList = taskList;
    }
    @Override
    public TasksAdapter.TasksViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        return new TasksViewHolder(ItemTaskListBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false));
    }

    @Override
    public void onBindViewHolder(TasksViewHolder holder, int position) {
        Task task = taskList.get(position);
        holder.textViewTask.setText(task.getTask());
        holder.textViewDesc.setText(task.getDesc());
        holder.textViewFinishBy.setText(task.getFinishBy());

        if (task.isFinished())
            holder.textViewStatus.setText("Completed");

        else
            holder.textViewStatus.setText("Not Completed");

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
              Task task = taskList.get(position);
              Intent intent = new Intent(mCtx, UpdateTaskActivity.class);
              intent.putExtra("task", task);
              mCtx.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return taskList.size();
    }

    class TasksViewHolder extends RecyclerView.ViewHolder {

        TextView textViewStatus, textViewTask, textViewDesc, textViewFinishBy;

        public TasksViewHolder(@NonNull ItemTaskListBinding itemTaskListBinding) {
            super(itemTaskListBinding.getRoot());

            textViewStatus = itemTaskListBinding.textViewStatus;
            textViewTask = itemTaskListBinding.textViewTask;
            textViewDesc = itemTaskListBinding.textViewDesc;
            textViewFinishBy = itemTaskListBinding.textViewFinishBy;

        }
    }
}
