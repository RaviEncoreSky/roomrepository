package com.example.roomdatabaseexample;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;

import com.example.roomdatabaseexample.adapter.TasksAdapter;
import com.example.roomdatabaseexample.baseActivity.BaseAppActivity;
import com.example.roomdatabaseexample.databinding.ActivityRoomDashboardBinding;
import com.example.roomdatabaseexample.roomDatabaseFiles.DatabaseClient;
import com.example.roomdatabaseexample.roomDatabaseFiles.Task;

import java.util.List;

public class RoomDashboard extends BaseAppActivity {

    RecyclerView recyclerView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ActivityRoomDashboardBinding dashboardBinding = ActivityRoomDashboardBinding.inflate(getLayoutInflater());
        setContentView(dashboardBinding.getRoot());

        recyclerView = dashboardBinding.recyclerViewTask;
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        dashboardBinding.btnAddNewTask.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent  = new Intent(RoomDashboard.this,AddTaskActivity.class);
                startActivity(intent);
            }
        });
        getTasks();
    }

    private void getTasks() {

        class GetTasks extends AsyncTask<Void, Void, List<Task>> {

            @Override
            protected List<Task> doInBackground(Void... voids) {
                List<Task> taskList = DatabaseClient
                        .getInstance(getApplicationContext())
                        .getAppDatabase()
                        .taskDao()
                        .getAll();
                return taskList;
            }

            @Override
            protected void onPostExecute(List<Task> tasks) {
                super.onPostExecute(tasks);
                TasksAdapter adapter = new TasksAdapter(RoomDashboard.this, tasks);
                recyclerView.setAdapter(adapter);
            }
        }

        GetTasks gt = new GetTasks();
        gt.execute();
    }
}