package com.example.roomdatabaseexample.utils;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;

import androidx.appcompat.app.AlertDialog;

public class AlertDialogHelper {

    public static void showAlertDialog(Context context, String title, String message, final TwoOptionCallBack callBack) {

        AlertDialog.Builder builder;
        builder = new AlertDialog.Builder(context);
        builder.setMessage(message);
        builder.setTitle(title);
        builder.setCancelable(false);

        builder.setPositiveButton("YES", new DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                callBack.onPositiveAction(builder.show());


            }
        });

        builder.setNegativeButton("NO", new DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface dialog, int which)
            {
                callBack.onNegativeAction(builder.show());
            }
        }).show();

    }


    public interface TwoOptionCallBack {
        void onPositiveAction(Dialog dialog);
        void onNegativeAction(Dialog dialog);
    }
}
