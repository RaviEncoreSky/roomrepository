package com.example.roomdatabaseexample.baseActivity;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.os.Bundle;

import com.example.roomdatabaseexample.databinding.ActivityBaseAppBinding;
import com.example.roomdatabaseexample.utils.AlertDialogHelper;

public class BaseAppActivity extends AppCompatActivity {

    ProgressDialog pDialog;
    AlertDialogHelper alertDialogHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ActivityBaseAppBinding baseAppBinding = ActivityBaseAppBinding.inflate(getLayoutInflater());
        setContentView(baseAppBinding.getRoot());

    }

    void displayProgressDialog() {
        pDialog = new ProgressDialog(this);
        pDialog.setMessage("Loading..."); // Setting Message
        pDialog.setTitle("Progress Dialog"); // Setting Title
        pDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER); // Progress Dialog Style Spinner
        pDialog.show(); // Display Progress Dialog
        pDialog.setCancelable(false);

    }
}