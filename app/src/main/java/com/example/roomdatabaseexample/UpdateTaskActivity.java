package com.example.roomdatabaseexample;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Dialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.example.roomdatabaseexample.baseActivity.BaseAppActivity;
import com.example.roomdatabaseexample.databinding.ActivityUpdateTaskBinding;
import com.example.roomdatabaseexample.roomDatabaseFiles.DatabaseClient;
import com.example.roomdatabaseexample.roomDatabaseFiles.Task;
import com.example.roomdatabaseexample.utils.AlertDialogHelper;

public class UpdateTaskActivity extends BaseAppActivity {
    Task task;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ActivityUpdateTaskBinding updateTaskBinding = ActivityUpdateTaskBinding.inflate(getLayoutInflater());
        setContentView(updateTaskBinding.getRoot());

        task= (Task) getIntent().getSerializableExtra("task");
        updateTaskBinding.editTextTask.setText(task.getTask());
        updateTaskBinding.editTextDesc.setText(task.getDesc());
        updateTaskBinding.editTextFinishBy.setText(task.getFinishBy());
        updateTaskBinding.checkBoxFinished.setChecked(task.isFinished());

        updateTaskBinding.buttonUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final String sTask = updateTaskBinding.editTextTask.getText().toString().trim();
                final String sDesc = updateTaskBinding.editTextDesc.getText().toString().trim();
                final String sFinishBy = updateTaskBinding.editTextFinishBy.getText().toString().trim();

                if (sTask.isEmpty()) {
                    updateTaskBinding.editTextTask.setError("Task required");
                    updateTaskBinding.editTextTask.requestFocus();
                    return;
                }

                if (sDesc.isEmpty()) {
                    updateTaskBinding.editTextDesc.setError("Desc required");
                    updateTaskBinding.editTextDesc.requestFocus();
                    return;
                }

                if (sFinishBy.isEmpty()) {
                    updateTaskBinding.editTextFinishBy.setError("Finish by required");
                    updateTaskBinding.editTextFinishBy.requestFocus();
                    return;
                }

                Boolean  checked = false;
                if(updateTaskBinding.checkBoxFinished.isChecked())
                {
                  checked = true;
                }

                updateTask(sTask,sDesc,sFinishBy,checked);

            }
        });

        updateTaskBinding.buttonDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialogHelper.showAlertDialog(UpdateTaskActivity.this,
                        "Delete Warning",
                        "Task will be Deleted. Continue ?",
                        new AlertDialogHelper.TwoOptionCallBack()
                        {
                    @Override
                    public void onPositiveAction(Dialog dialog) {
                        dialog.cancel();
                        deleteTask(task);

                    }

                    @Override
                    public void onNegativeAction(Dialog dialog) {
                        dialog.cancel();

                    }
                });

            }
        });

    }

    private void updateTask(String sTask, String sDesc, String sFinishBy,Boolean checked) {

        class UpdateTask extends AsyncTask<Void, Void, Void> {

            @Override
            protected Void doInBackground(Void... voids) {
                task.setTask(sTask);
                task.setDesc(sDesc);
                task.setFinishBy(sFinishBy);
                task.setFinished(checked);
                DatabaseClient.getInstance(getApplicationContext()).getAppDatabase()
                        .taskDao()
                        .update(task);
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                Toast.makeText(getApplicationContext(), "Updated", Toast.LENGTH_LONG).show();
                finish();
                startActivity(new Intent(UpdateTaskActivity.this, RoomDashboard.class));
            }
        }

        UpdateTask ut = new UpdateTask();
        ut.execute();
    }

    private void deleteTask(Task task) {
        class DeleteTask extends AsyncTask<Void, Void, Void> {

            @Override
            protected Void doInBackground(Void... voids) {
                DatabaseClient.getInstance(getApplicationContext()).getAppDatabase()
                        .taskDao()
                        .delete(task);
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                Toast.makeText(getApplicationContext(), "Task Deleted", Toast.LENGTH_LONG).show();
                finish();
                startActivity(new Intent(UpdateTaskActivity.this, RoomDashboard.class));
            }
        }

        DeleteTask dt = new DeleteTask();
        dt.execute();
    }
}